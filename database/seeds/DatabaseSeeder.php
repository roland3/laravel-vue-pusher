<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User1',
            'email' => 'user1@networld.hk',
            'password' => bcrypt('123456'),
            'api_token' => str_random(60),
            'remember_token' => str_random(10),
        ]);

        DB::table('users')->insert([
            'name' => 'User2',
            'email' => 'user2@networld.hk',
            'password' => bcrypt('123456'),
            'api_token' => str_random(60),
            'remember_token' => str_random(10),
        ]);


        /*
        factory(App\User::class, 25)->create()->each(function ($u) {
            for ($i=0; $i <= 3; $i++) {
                $u->posts()->save(factory(App\Post::class)->make());
            }
        });
        */
    }
}
